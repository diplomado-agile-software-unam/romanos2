package com.iram.core;
import com.iram.test.Tester;

public class Main {

	public static void main(String[] args) {
		
		Converter converter = new Converter();
		Tester tester = new Tester();
		tester.Test(converter); //refactoring Extract Method
		
	}

}

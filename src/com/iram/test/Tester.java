package com.iram.test;
import com.iram.core.Converter;

public class Tester {
	
	/*
	 Clase de equivalencia 1: numeros menores a 1
	 Clase de equivalencia 2: numeros mayores iguales a 1 pero menores iguales a 20
	 Clase de equivalencia 3: numeros mayores a 20
	 Clase de equivalencia 4: entrada que no es numero 
	 
	 Valores Limite Clase de equivalencia 1: 0,1
	 Valores Limite Clase de equivalencia 2: 0,1,2,19,20,21
	 Valores Limite Clase de equivalencia 3: 20,30,86
	 Valores Limite Clase de equivalencia 4: "Juan perez"
	 
	 */
	
	public void Test(Converter converter)
	{
		System.out.println(testN0(converter.convierteARomano(0)));
        System.out.println(testN1(converter.convierteARomano(1)));
        System.out.println(testN2(converter.convierteARomano(2)));
        System.out.println(testN19(converter.convierteARomano(19)));
        System.out.println(testN20(converter.convierteARomano(20)));
        System.out.println(testN21(converter.convierteARomano(21)));
        System.out.println(testN30(converter.convierteARomano(30)));
        System.out.println(testN86(converter.convierteARomano(86)));
	}
	
	public static Boolean testN0(String result) {
		if(result.equals("Error"))
			return true;
		return false;
	}

	public static Boolean testN1(String result) {
		if(result.equals("I"))
			return true;
		return false;
	}
	
	public static Boolean testN2(String result) {
		if(result.equals("II"))
			return true;
		return false;
	}
	
	public static Boolean testN19(String result) {
		if(result.equals("XIX"))
			return true;
		return false;
	}
	
	public static Boolean testN20(String result) {
		if(result.equals("XX"))
			return true;
		return false;
	}
	
	public static Boolean testN21(String result) {
		if(result.equals("Error"))
			return true;
		return false;
	}
	
	public static Boolean testN30(String result) {
		if(result.equals("Error"))
			return true;
		return false;
	}
	
	public static Boolean testN86(String result) {
		if(result.equals("Error"))
			return true;
		return false;
	}
	
	public static Boolean testNotNum(String result) {
		if(result.equals("Error"))
			return true;
		return false;
	}
}
